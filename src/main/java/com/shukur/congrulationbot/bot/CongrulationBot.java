package com.shukur.congrulationbot.bot;

import com.shukur.congrulationbot.congrulation.BirthdayCongrulation;
import com.shukur.congrulationbot.congrulation.ChildCongrulation;
import com.shukur.congrulationbot.congrulation.WeddingCongrulation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Component
public class CongrulationBot extends TelegramLongPollingBot {

    private static final Logger LOG = LoggerFactory.getLogger(CongrulationBot.class);
    private static final String START = "/start";
    private static final String WEDDING = "/wedding";
    private static final String BIRTHDAY = "/birthday";
    private static final String CHILD = "/child";

    public CongrulationBot(@Value("${bot.token}") String botToken) {

        super(botToken);
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (!update.hasMessage() || !update.getMessage().hasText()) {
            return;
        }

        var message = update.getMessage().getText();
        var chatId = update.getMessage().getChatId();

        switch (message) {
            case START -> {
                String userName = update.getMessage().getChat().getUserName();
                startCommand(chatId, userName);
            }
            case WEDDING -> weddingCommand(chatId);
            case BIRTHDAY -> birthdayCommand(chatId);
            case CHILD -> childCommand(chatId);
            default -> unknownCommand(chatId);
        }
    }

    private void startCommand(Long chatId, String userName) {
        var text = """
                Добро пожаловать в бот!
                
                Посредством этого бота вы можете сгенерировать поздравления для различных событий.
                
                Для этого укажите событие для которого вам нужно поздравление:
                1. /wedding - поздравления для молодожен
                2. /birthday - поздравления для именниника
                3. /child - поздравления для родителей новорожденного
                
                Удачи в дальнейших действиях!
               
                """;
        var formattedText = String.format(text, userName);
        sendMessage(chatId, formattedText);
    }

    private void weddingCommand(Long chatId) {
        WeddingCongrulation weddingCongrulation = new WeddingCongrulation();
        String randomText = weddingCongrulation.getRandomText();

        var formattedText = String.format(randomText);
        sendMessage(chatId, formattedText);
    }

    private void birthdayCommand(Long chatId) {
        BirthdayCongrulation birthdayCongrulation = new BirthdayCongrulation();
        String randomText = birthdayCongrulation.getRandomText();

        var formattedText = String.format(randomText);
        sendMessage(chatId, formattedText);
    }

    private void childCommand(Long chatId) {
        ChildCongrulation childCongrulation = new ChildCongrulation();
        String randomText = childCongrulation.getRandomText();

        var formattedText = String.format(randomText);
        sendMessage(chatId, formattedText);
    }

    private void unknownCommand(Long chatId) {
        var text = """
        Выберите одну из команд указанных в списке: 
        
        1. /wedding
        2. /birthday
        3. /child
        
        Удачи в дальнейших действиях!
       
        """;
        sendMessage(chatId, text);
    }

    @Override
    public String getBotUsername() {
        return "Sh_Programming_Lab_Project_3_Bot";
    }

    private void sendMessage(Long chatId, String text) {
        var chatIdStr = String.valueOf(chatId);
        var sendMessage = new SendMessage(chatIdStr,text);
        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            LOG.error("Something wrong with mail sender", e);
        }
    }
}
