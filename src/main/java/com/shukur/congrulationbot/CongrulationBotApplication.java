package com.shukur.congrulationbot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CongrulationBotApplication {

	public static void main(String[] args) {
		SpringApplication.run(CongrulationBotApplication.class, args);
	}

}
