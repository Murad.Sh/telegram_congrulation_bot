package com.shukur.congrulationbot.configuration;

import com.shukur.congrulationbot.bot.CongrulationBot;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

@Configuration
public class CongrulationBotConfiguration {
    @Bean
    public TelegramBotsApi telegramBotsApi(CongrulationBot congrulationBot) throws TelegramApiException {
        var api = new TelegramBotsApi(DefaultBotSession.class);
        api.registerBot(congrulationBot);
        return api;
    }
}
